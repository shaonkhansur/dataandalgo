import Stack from './Stack';

const stack = new Stack();

export default class Expression {

    private expString: string;

    constructor(exp: string) {
        this.expString = exp;
    }


    isValidExpression(): boolean {
        const expCharArray = this.expString.split('');
        for(let i = 0; i < expCharArray.length; i++) {
            if( expCharArray[i] === '(' || 
                expCharArray[i] === '{' ||
                expCharArray[i] === '[' ||
                expCharArray[i] === '<' ) {
                stack.push(expCharArray[i]);
            }

            if (    expCharArray[i] === ')' || 
                    expCharArray[i] === '}' ||
                    expCharArray[i] === ']' ||
                    expCharArray[i] === '>' ) {
                if (stack.isEmpty()) {
                    stack.push(expCharArray[i]);
                } else {
                    const top = stack.peak();
                    if (    top === '(' && expCharArray[i] != ')' ||
                            top === '{' && expCharArray[i] != '}' ||
                            top === '[' && expCharArray[i] != ']' ||
                            top === '<' && expCharArray[i] != '>') {
                                stack.push(expCharArray[i]);

                    } else {
                        stack.pop();
                    }
                }
            }
        }

        return stack.isEmpty();

    }
}