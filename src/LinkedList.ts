class Node {
    value: number;
    next: Node;

    constructor(value: number) {
        this.value = value;
        this.next = null;
    }
}

class LinkedList {
    first: Node;
    last: Node;
    size: number;
    constructor() {
        this.size = 0;
    }

    addLast(value: number) {
        let node = new Node(value);

        if (!this.first) {
            this.first = this.last = node;
            this.size++;
        } else {
            this.last.next = node;
            this.last = node;
            this.size++;
        }
    }

    addFirst(value: number) {
        if (!this.first) {
            this.addLast(value);
        } else {
            let node = new Node(value);
            node.next = this.first;
            this.first = node;
            this.size++;
            
        }
    }

    deleteFirst() {
        if (!this.first) {
            return -1;
        } else {
            let current = this.first;
            current = this.first.next;
            this.first = current;
        }
    }

    deleteLast() {
        if (!this.last) {
            return -1
        } else {
            let current = this.first;
            while(current.next.next != null) {
                current = current.next

            }
            this.last = current;
            this.last.next = null;
            this.size--;
        }
    }

    removeAt(index: number) {
        if(index < 0 && index > this.size) {
            return -1;
        }

        if (index === 0) {
            this.deleteFirst();
            return;
        }
        let count = 0;
        if(!this.first) {
            return -1;
        } else {
            let current = this.first;
            while (current) {
                count++;
                if (count === index) {
                    let prevNode = current;
                    let targetNode = current.next;
                    let nextNode = current.next.next;
                    if (nextNode === null) {
                        this.last = prevNode;
                    }
                    prevNode.next = nextNode;
                    this.size--;
                    break;
                }
                current = current.next;
            }
        }
        

    }

    indexOf(value: number): number {
        if (!this.first) {
            return;
        } else {
            let current = this.first;
            let index = 0;
            while (current != null) {
                if (value === current.value) {
                    return index;

                }
                current = current.next;
                index++;
            }
            return -1;
            
        }
    }

    contains(value: number): boolean {
        if (this.indexOf(value) === -1) {
            return false;
        } else {
            return true;
        }
    }

    reverse(): void {
        
        let prev = this.first;
        let current = this.first.next;

        
        while(current != null) {
            let next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        let last = this.first
        this.first = prev;
        this.last = last;
        last.next = null;
    }
    // k = 1
    //  1 -> 2 -> 3 -> 4 -> 5
    //  *                   *         

    findKthNumberFromEnd(k: number) {
        let a = this.first;
        let b = this.first;
        
        for (let i = 0; i < k - 1; i++) {
            if(b.next != null) {
                b = b.next;
            }
        }

        while (b != this.last) {
            // debugger;
            a = a.next;
            b = b.next;
        }

        return a.value;


        
    }

    print() {
        if (!this.first) {
            return;
        } else {
            let current = this.first;
            while(current) {
                console.log(current.value);
                current = current.next;
            }
        }
    }
}

export default LinkedList;