import Stack from './Stack';

const stack = new Stack();
export default class Queue {
    
    private dataStore: number[] = [];

    constructor(array: number[]) {
        this.dataStore = array;
    }

    add(item: number): void {
        this.dataStore.push(item);
    }

    remove(): number {
        const item = this.dataStore[0];
        this.dataStore.splice(0, 1);
        return item;

    }

    reverse() {
        this.dataStore.map(el => stack.push(el));
        this.dataStore = [];
        while(!stack.isEmpty()) {
            this.dataStore.push(stack.pop());
        }

    }

    isEmpty(): boolean {
        return this.dataStore.length === 0; 
    }

    print(): void {
        console.log(this.dataStore);
    }
}