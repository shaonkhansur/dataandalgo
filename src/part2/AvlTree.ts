class AVLNode {
    value: number = null;
    leftChild: AVLNode = null;
    rightChild: AVLNode = null;

    constructor(value: number) {
        this.value = value;
    }
}


export default class AvlTree {
    

    private root: AVLNode;

    constructor() {

    }


    public insert(value: number): void {
        // debugger;
        this.root = this.insertNode(this.root, value);
    }

    private insertNode(root: AVLNode, value: number): AVLNode {
        if (root == undefined) return new AVLNode(value);

        if (value < root.value) {
            root.leftChild = this.insertNode(root.leftChild, value);
        } else {
            root.rightChild = this.insertNode(root.rightChild, value);
        }

        return root;


    }


    public getTree(): AVLNode {
        return this.root;
    }
    
    


}