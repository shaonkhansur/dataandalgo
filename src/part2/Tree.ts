

class Node {

    value: number = null;
    leftChild: Node = null;
    rightChild: Node = null;

    constructor(value: number) {
        this.value = value;
    }
}

export default class Tree {

    private root: Node;

    constructor() {
        // this.root = new Node();
    }

    insert(value: number): void {
        const node = new Node(value);

        if (this.root == undefined) {
            this.root = new Node(value);
            return;
        }

        let current: Node = this.root;

        while(true) {
            if (value < current.value) {
                if (current.leftChild == null) {
                    current.leftChild = node;
                    break;
                }
                current = current.leftChild;

            } else {
                if (current.rightChild == null) {
                    current.rightChild = node;
                    break;
                }
                current = current.rightChild;
            }
        } 
        

    }

    find(value: number) {
        let current: Node = this.root;

        if (value === current.value) {
            return true;
        }

        while (current != null) {
            if (value < current.value) {
                current = current.leftChild;
            } else if (value > current.value) {
                current = current.rightChild
            } else {
                return true;
            }
        }
        return false;
    }
    preOrderTraverse(): void {
        this.traversePreOrder(this.root);
    }

    inOrderTraverse(): void {
        this.traverseInOrder(this.root);
    }

    portOrderTraverse(): void {
        this.traversePostOrder(this.root);
    }

    private traversePreOrder(root: Node): void {
        if (root === null) {
            return;
        }

        console.log(root.value);
        this.traversePreOrder(root.leftChild);
        this.traversePreOrder(root.rightChild);
    }



    private traverseInOrder(root: Node): void {
        if (root === null) {
            return;
        }

        this.traverseInOrder(root.leftChild);
        console.log(root.value);
        this.traverseInOrder(root.rightChild);
    }

    private traversePostOrder(root: Node): void {
        if (root === null) {
            return;
        }

        this.traversePostOrder(root.leftChild);
        this.traversePostOrder(root.rightChild);
        console.log(root.value);
    }

    public treeHeight(): number {
       return this.height(this.root);
    } 

    private isLeaf(root: Node): boolean {
        return root.leftChild == null && root.rightChild == null;
    }

    private height(root: Node): number {
        if (root == undefined)
            return -1;

        if (this.isLeaf(root)) {
            return 0;
        }
        return 1 + Math.max(this.height(root.leftChild), this.height(root.rightChild));
    }

    treeMin(): number {
        return this.min(this.root);
    }

    binarySearchTreeMinValue(): any {
        if (this.root == undefined)
            throw new Error('IllegalExpression()');
        let current = this.root;
        let last = current;

        while(current != null) {
            last = current;
            current = current.leftChild
        }

        return last.value
    }

    binarySearchTreeMaxValue(): any {
        if (this.root == undefined)
            throw new Error('illegalExpression()');
        let current = this.root;
        let max = current;

        while (current != null) {
            max = current;
            current = current.rightChild
        }

        return max.value;
    }

    private min(root: Node): number {

        if (root.leftChild == null || root.rightChild == null) {
            return root.value;
        }
 
        let left = this.min(root.leftChild);
        let right = this.min(root.rightChild);
        return Math.min(Math.min(left, right), root.value)
    }

    equal(second: Tree): boolean {
        return this.isEqual(this.root, second.root);
    }

    private isEqual(first: Node, second: Node): boolean {

        if (first == null && second == null) {
            return true;
        }

        if (first != null && second != null) {
            return first.value === second.value && this.isEqual(first.leftChild, second.leftChild) && this.isEqual(first.rightChild, second.rightChild) 
        }

        return false;
    }
    

    getTree() {
        return this.root;
    }

}

