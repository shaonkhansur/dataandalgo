import Stack from './Stack';

export default class ReversedString {
    string: string = '';

    constructor(str: string) {
        this.string = str;
    }



    reversed(): string {
        const stack  = new Stack();
        let reversedStr = '';
        for(let i = 0; i < this.string.length; i++) {
            stack.push(this.string.charAt(i));
        }

        while(!stack.isEmpty()) {
            reversedStr = reversedStr + stack.pop();
        }
        return reversedStr;
    }
}