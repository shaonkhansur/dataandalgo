import Stack from './Stack';
import Queue from './Queue';

const stack1 = new Stack();
const stack2 = new Stack();
const queue = new Queue([]);

export default class QueueWithTwoStack {


    enqueue(item: number): void {
        stack1.push(item);
    }

    dequeue(): number {
        if (stack2.isEmpty()) {
            while(!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }

        return stack2.pop();
    }

}