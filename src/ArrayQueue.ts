

export default class ArrayQueue {

    private items: number[] = [];
    private arrayLengthLimit: number = 0;

    private rear: number = 0;
    private front: number = 0;
    private count: number = 0;

    constructor(length: number) {
        this.arrayLengthLimit = length;
        for (let i = 0; i < this.arrayLengthLimit; i++) {
            this.items.push(0);
        }
    }

    enqueue(item: number): void {
        if (this.count === this.items.length) {
            throw new Error('IllegalStateException()');
        }
        this.items[this.rear] = item;
        this.rear = ( this.rear + 1 ) % this.items.length;
        this.count++;
    }

    dequeue(): number {
        const item = this.items[this.front];
        this.items[this.front] = 0;
        this.front = (this.front + 1) % this.items.length;
        this.count--;

        return item;
    }


    print(): void {
        console.log('front', this.front);
        console.log('rear', this.rear);
        console.log('count', this.count);
        console.log(this.items);
    }



}