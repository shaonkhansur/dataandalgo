

export default class Stack {
    private dataStore: any[] = [];
    private count: number = 0;
    constructor() {

    }

    push(item: any): void {
        this.dataStore.push(item);
        this.count++;
    }

    pop(): any {
        const item = this.dataStore[this.count - 1];
        this.dataStore.splice(this.count-1, 1);
        this.count--;
        return item;
    }

    peak(): any {
        const item = this.dataStore[this.count - 1];
        return item;
    }

    isEmpty(): boolean {
        return this.dataStore.length === 0;
    }

    print(): void {
        console.log(this.dataStore);
    }

    size(): number {
        return this.count;
    }


}